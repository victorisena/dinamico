<?php
    require_once("config.php");

    //trabalhando com datas
    // string date(string $format[, int $timestamp = time()]);
    echo date('d-m-Y').'<br />';
    echo date('H:i:s').'<br />';
    echo date_default_timezone_get().'<br />';
    echo date('l w d-m-Y').'<br />';
    echo date(DATE_RSS).'<br />';

    $timestamp = time();
    // 1568935071
    echo $timestamp .'<br />';
    echo date('D, d-M-Y H:i:s',0 ) .'<br />';

    $timestamp = mktime(12,30,00,06,26,2002);
    echo $timestamp;
    echo date('d-m-Y H:i:s', $timestamp)."<br>";
    //Recebe data e hora do usuário 
    $data_digitada = '09-10-1998 18:25:05';
    //Converte data e hora recebida em timestamp
    $timestamp1 = strtotime($data_digitada);
    echo $timestamp1."<br />";
    echo date('d/m/Y H:i:s', $timestamp1)."<br />";

    $timestamp2 = strtotime('+1 year', $timestamp1);
    echo $timestamp1."<br />";
    echo date('d/m/Y H:i:s', $timestamp2)."<br />";

    $data_padrao = new DateTime();
    // print_r($data_padrao);
    $data_fin = new DateTime('17-09-2019 15:32:09');
    $data_amanha = new DateTime('+1 day');

    echo $data_fin->format('d m Y');
?>