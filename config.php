<?php

// Inicializar sessão de Usuário

// session_start();

// Definindo padrão de Zona GMT (TimeZone) -3,00

date_default_timezone_set('America/Sao_Paulo');

// Inicializar carregamentos das classes do projeto
spl_autoload_register(function($nome_classe){
    $nome_arquivo = "class".DIRECTORY_SEPARATOR.$nome_classe.".php";
    if(file_exists($nome_arquivo)){
        require_once($nome_arquivo);
    }
});

//Criar Constante 

define('IP_SERVER_DB', '127.0.0.1');
define('HOSTNAME','ITQ0626036W10-1');
define('NOME_BANCO','dinamicodb');
define('USER_DB','root');
define('PASS_DB','');
define('CHARSET', 'utf8');

?>