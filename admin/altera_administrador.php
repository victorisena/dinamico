<?php
    $id = filter_input(INPUT_GET,'id');
    $nome = filter_input(INPUT_GET,'nome');
    $email = filter_input(INPUT_GET,'email');
    $login = filter_input(INPUT_GET,'login');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alteração de Administrador</title>
    <link rel="stylesheet" href="css/style_admin.css">
</head>
<body>
    <form action="op_administrador.php" method="get" enctype="multipart/form-data">
        <fieldset>
            <legend>Alteração de Administrador</legend>
            <div>
                <input type="hidden" name="id" value="<?php echo $id;?>">
            </div>
            <label for="">Nome</label>
            <div>
                <input type="text" name="nome" value="<?php echo $nome;?>">
            </div>
            <label for="">Email</label>
            <div>
                <input type="email" name="email" value="<?php echo $email;?>">
            </div>
            <label for="">Login</label>
            <div>
               <input type="text" name="login" value="<?php echo $login;?>">
            </div>
            <div>
               <input type="submit" name="alterar" value="Registrar Alteração">
            </div>
        </fieldset>
    </form>
</body>
</html>