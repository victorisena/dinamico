<?php
require_once("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Site Dinâmico</title>
    <link rel="stylesheet" href="css/style_admin.css">
</head>
<body>
    <div id="box-cadastro">
        <div id="formulario-menor">
            <form id="frmbanner" name="frmbanner" action="op_banner.php" method="post">
                <fieldset>
                    <legend>Cadastro Banner</legend>
                    <?php 
                        require_once("../config.php");
                        $admins = Administrador::getList();
                    ?>
                    <select name="categoria" id="categoria">
                        <?php 
                            foreach ($admins as $adm) {
                        ?> 
                            <option value="<?php echo $adm['id'].'">'.$adm['nome'];?></option>
                        <?php }?>
                    </select>
                    <label for="">
                        <span>Titulo</span> 
                        <input type="text" name="titulo" id="titulo" value="">
                    </label>
                    <label for="">
                        <span>Link</span> 
                        <input type="text" name="link" id="link" value="">
                    </label>
                    <label for="">
                        <span>Imagem</span> 
                        <input type="text" name="imagem" id="imagem" value="">
                    </label>
                    <label for="">
                        <span>Alt</span> 
                        <input type="text" name="alt" id="alt" value="">
                    </label>
                    <label for="">
                        <span>Ativo</span>
                        <input type="text" name="ativo" id="ativo" value="">
                    </label>
                    <br>
                    <input type="submit" value="Gravar" name="gravar" class="botao">
                </fieldset>
            </form>
        </div>
    </div>
</body>
</html>